import React from "react"
import { Row, Col } from "reactstrap"
import styled from "styled-components"

import Items from "./Items.jsx"

const Publicaciones = styled.div`
    height: 1000px;
    background-color: black;
    padding-inline-start: -40px;

`;
const EspacioCabecera = styled.div`
    height: 70px;
`;

const MuroObj = () => {
    return (
        <>
            <Row>
                <Col>
                    <EspacioCabecera />
                </Col>
            </Row>
            <Row>
                <Col>
                    <Publicaciones>
                        <Items />
                    </Publicaciones>
                </Col>
            </Row>
        </>
    );
};

export default MuroObj;