import React from "react"
import { Row, Col } from "reactstrap"
import styled from "styled-components"

const PieStyle = styled.div`
    color: white;
    background-color: dimgrey;
    text-align: center;
    padding 1% 2%;
    width: 100%;
    position: fixed;
    bottom: 0px;
`;

const AddIcon = styled.i`
    cursor: pointer;
`;

const PieObj = () => {
    return (
        <Row>
            <Col>
                <PieStyle>
                    <h1><AddIcon className="fa fa-plus-square" aria-hidden="true"></AddIcon> </h1>
                </PieStyle>
            </Col>
        </Row>
    );
};

export default PieObj;