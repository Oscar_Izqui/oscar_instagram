import './App.css';
import styled from "styled-components"

import Cabecera from "./Cabecera.jsx"
import Muro from "./Muro.jsx"
import Pie from "./Pie.jsx"

const Contenedor = styled.div`

`;

function App() {
  return (
    <div className="App">
      <Contenedor>
        <Muro />
        <Cabecera />
        <Pie />
      </Contenedor>
    </div>
  );
}

export default App;
