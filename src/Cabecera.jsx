import React from "react"
import { Row, Col } from "reactstrap"
import styled from "styled-components"

const CabeceraStyle = styled.div`
    color: white;
    background-color: dimgrey;
    text-align: left;
    width: 100%;
    position: fixed;
    top: 0px;
`;

const CabeceraObj = () => {
    return (
        <Row>
            <Col>
                <CabeceraStyle>
                    <h1><i className="fa fa-camera-retro" aria-hidden="true"></i> Instagram</h1>
                </CabeceraStyle>
            </Col>
        </Row>
    );
};

export default CabeceraObj;