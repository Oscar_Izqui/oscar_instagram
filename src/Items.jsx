import React, { useState } from "react";
import { v4 as uuid } from "uuid";
import styled from "styled-components";

const Imatge = styled.img`
  width: 100%;
`;

const Items = () => {
  const [items, setItems] = useState([]);
  const [url, setUrl] = useState();


  const guardar = () => {
    localStorage.setItem('mis_items', JSON.stringify(items));
  }

  const recuperar = () => {
    const itemsJson = localStorage.getItem('mis_items');
    const cosas = JSON.parse(itemsJson);
    if (cosas && cosas.length) {
      setItems(cosas);
    } else {
      setItems([]);
    }

  }

  const afegir = () => {
    if (url) {
      const nouItem = {
        imagen: url,
        id: uuid(),
      };
      setItems([...items, nouItem]);
      setUrl('');
    }

  };

  const tots = items.map((el) => (
    <Imatge key={el.id} src={el.imagen} alt="" />
  ));

  return (
    <>
      <input type="text" value={url} onChange={(ev) => setUrl(ev.target.value)} />
      <br />
      <button onClick={afegir}>Afegir</button>
      <button onClick={guardar}>Guardar datos</button>
      <button onClick={recuperar}>Leer datos</button>
      <br />
      <br />

      <ul>
        {tots}
      </ul>
    </>
  );
};

export default Items;
